package com.kzuromski.redditclone;

import com.kzuromski.redditclone.model.Post;
import com.kzuromski.redditclone.model.PostComment;
import com.kzuromski.redditclone.model.Tag;
import com.kzuromski.redditclone.model.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@SpringBootApplication
public class RedditCloneApplication implements CommandLineRunner {
    @PersistenceContext
    EntityManager entityManager;

    public static void main(String[] args) {
        SpringApplication.run(RedditCloneApplication.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {

        Tag tag = new Tag();
        tag.setName("Example tag");

        Post post = new Post();

        post.setAuthor(new User("waldek", "xinyao@op.pl", "waldek"));
        post.setSubject("Example subject");
        post.setContent("Lorem ipsum dolor");


        Post post2 = new Post();
        post2.setContent("Lorem");
        post2.setSubject("Something something");

        PostComment postComment = new PostComment();
        PostComment postComment1 = new PostComment();

        postComment.setContent("Comment number 1");
        postComment1.setContent("Comment number 2");

        post.addComment(postComment);
        post.addComment(postComment1);

        tag.addPost(post);
        tag.addPost(post2);
        entityManager.persist(tag);
        entityManager.persist(post);
    }
}
