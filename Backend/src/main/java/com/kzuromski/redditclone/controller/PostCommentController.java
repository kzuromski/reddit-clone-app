package com.kzuromski.redditclone.controller;

import com.kzuromski.redditclone.dto.postcomment.AddPostCommentDto;
import com.kzuromski.redditclone.dto.postcomment.EditPostCommentDto;
import com.kzuromski.redditclone.dto.postcomment.PostCommentDto;
import com.kzuromski.redditclone.exception.PostCommentNotFoundException;
import com.kzuromski.redditclone.model.PostComment;
import com.kzuromski.redditclone.service.PostCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/post")
public class PostCommentController {

    private PostCommentService postCommentsService;

    @Autowired
    public PostCommentController(PostCommentService postCommentService) {
        this.postCommentsService = postCommentService;
    }

    @GetMapping("/{postId}/comments")
    public List<PostCommentDto> getComments(@PathVariable Long postId) {
        return postCommentsService.getPostComments(postId);
    }

    @GetMapping("/{postId}/comments/{commentId}")
    public PostComment getComment(@PathVariable Long postId, @PathVariable Long commentId) {
        return postCommentsService.getComment(postId, commentId).orElseThrow(() -> new PostCommentNotFoundException(commentId));
    }

    @PostMapping("/{postId}")
    public AddPostCommentDto addComment(@PathVariable Long postId, @RequestBody AddPostCommentDto addPostCommentDto) {
        return postCommentsService.addComment(postId, addPostCommentDto);
    }

    @DeleteMapping("/{postId}/comments/{commentId}")
    public void deleteComment(@PathVariable Long postId, @PathVariable Long commentId) {
        postCommentsService.deleteComment(postId, commentId);
    }

    @PutMapping("/{postId}/comments/{commentId}")
    public ResponseEntity<PostComment> editComment(@PathVariable Long postId, @PathVariable Long commentId, @RequestBody EditPostCommentDto editPostCommentDto) {
        return postCommentsService.editComment(postId, commentId, editPostCommentDto);
    }

}
