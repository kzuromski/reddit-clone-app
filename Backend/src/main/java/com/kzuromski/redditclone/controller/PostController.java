package com.kzuromski.redditclone.controller;

import com.kzuromski.redditclone.dto.post.AddPostDto;
import com.kzuromski.redditclone.dto.post.EditPostDto;
import com.kzuromski.redditclone.dto.post.PostDto;
import com.kzuromski.redditclone.exception.PostNotFoundException;
import com.kzuromski.redditclone.model.Post;
import com.kzuromski.redditclone.repository.PostRepository;
import com.kzuromski.redditclone.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/post")
public class PostController {
    private PostService postService;
    private PostRepository postRepository;

    @Autowired
    public PostController(PostService postService, PostRepository postRepository) {
        this.postService = postService;
        this.postRepository = postRepository;
    }

    @GetMapping("")
    public List<PostDto> getPosts() {
        return postService.getAllPosts().orElse(new ArrayList<>());
    }

    @PostMapping("/add")
    public Post addPost(@RequestBody AddPostDto addPostDto) {
        return postService.save(addPostDto);
    }

    @GetMapping("/{id}")
    public Post getPost(@PathVariable Long id) {
        return postService.findOneById(id).orElseThrow(() -> new PostNotFoundException(id));
    }

    @PutMapping("/{id}")
    public Post updatePost(@PathVariable Long id, @RequestBody EditPostDto editPostDto) {
        return postService.updatePost(id, editPostDto).orElseThrow(() -> new PostNotFoundException(id));
    }

    @DeleteMapping("/{id}")
    public void deletePost(@PathVariable Long id) {
        postRepository.deleteById(id);
    }
}
