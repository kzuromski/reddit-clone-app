package com.kzuromski.redditclone.controller;

import com.kzuromski.redditclone.dto.post.PostDto;
import com.kzuromski.redditclone.dto.tag.EditTagDto;
import com.kzuromski.redditclone.dto.tag.TagDto;
import com.kzuromski.redditclone.exception.TagNotFoundException;
import com.kzuromski.redditclone.model.Tag;
import com.kzuromski.redditclone.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("tag")
public class TagController {
    private TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping("")
    public List<TagDto> getAllTags() {
        return tagService.getAllTags().orElse(new ArrayList<>());
    }

    @GetMapping("/{tagName}")
    public List<PostDto> getAllTagPosts(@PathVariable String tagName) {
        return tagService.getAllTagPosts(tagName).orElseThrow(() -> new TagNotFoundException(tagName));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/add")
    public Tag addTag(@RequestBody TagDto tagDto) {
        return tagService.addTag(tagDto);
    }

    @PutMapping("/{tagName}")
    public Tag editTag(@RequestBody EditTagDto editTagDto, @PathVariable String tagName) {
        return tagService.editTag(editTagDto, tagName).orElseThrow(() -> new TagNotFoundException(tagName));
    }
}
