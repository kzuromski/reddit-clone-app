package com.kzuromski.redditclone.dto.post;

import com.kzuromski.redditclone.model.Tag;
import lombok.Data;

import java.util.Date;

@Data
public class AddPostDto {
    private String author;
    private Tag tag;
    private String subject;
    private String content;
    private Date date;
}
