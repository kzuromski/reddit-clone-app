package com.kzuromski.redditclone.dto.post;

import lombok.Data;

import java.util.Date;

@Data
public class EditPostDto {
    private String subject;
    private String content;
    private Date date;
}
