package com.kzuromski.redditclone.dto.postcomment;

import lombok.Data;

import java.util.Date;

@Data
public class AddPostCommentDto {
    private String author;
    private String content;
    private Date date;
}
