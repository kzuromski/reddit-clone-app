package com.kzuromski.redditclone.dto.postcomment;

import lombok.Data;

import java.util.Date;

@Data
public class PostCommentDto {
    private String author;
    private String content;
    private Date date;
}
