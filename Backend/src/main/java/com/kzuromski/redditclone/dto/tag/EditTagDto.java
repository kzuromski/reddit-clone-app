package com.kzuromski.redditclone.dto.tag;

import lombok.Data;

@Data
public class EditTagDto {
    private String description;
}
