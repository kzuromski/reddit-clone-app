package com.kzuromski.redditclone.dto.tag;

import lombok.Data;

@Data
public class TagDto {
    private String name;
    private String description;
}
