package com.kzuromski.redditclone.exception;

public class PostCommentNotFoundException extends RuntimeException {
    public PostCommentNotFoundException(Long id) {
        super("Post comment with id: " + id + " was not found.");
    }
}
