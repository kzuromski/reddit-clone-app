package com.kzuromski.redditclone.exception;

public class PostNotFoundException extends RuntimeException {
    public PostNotFoundException(Long id) {
        super("Post with id: " + id + " was not found.");
    }
}
