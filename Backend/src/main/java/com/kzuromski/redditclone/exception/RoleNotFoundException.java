package com.kzuromski.redditclone.exception;

public class RoleNotFoundException extends RuntimeException {
    public RoleNotFoundException() {
        super("Role was not found.");
    }
}
