package com.kzuromski.redditclone.exception;

public class TagNotFoundException extends RuntimeException {
    public TagNotFoundException(String tagName) {
        super("Tag with name:{ " + tagName + " } was not found.");
    }
}
