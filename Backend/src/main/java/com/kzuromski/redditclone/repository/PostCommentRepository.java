package com.kzuromski.redditclone.repository;

import com.kzuromski.redditclone.model.PostComment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostCommentRepository extends JpaRepository<PostComment, Long> {
}
