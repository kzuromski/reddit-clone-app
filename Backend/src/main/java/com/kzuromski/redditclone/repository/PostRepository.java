package com.kzuromski.redditclone.repository;

import com.kzuromski.redditclone.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostRepository extends JpaRepository<Post, Long> {
    List<Post> findByTag(String tagName);
}
