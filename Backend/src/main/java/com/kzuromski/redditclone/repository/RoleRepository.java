package com.kzuromski.redditclone.repository;

import com.kzuromski.redditclone.enums.RoleName;
import com.kzuromski.redditclone.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByRoleName(RoleName roleName);
}
