package com.kzuromski.redditclone.service;

import org.springframework.security.core.userdetails.UserDetails;

public interface CustomUserDetailsService {
    UserDetails loadUserById(Long id);
}
