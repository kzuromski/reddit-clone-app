package com.kzuromski.redditclone.service;

import com.kzuromski.redditclone.dto.postcomment.AddPostCommentDto;
import com.kzuromski.redditclone.dto.postcomment.EditPostCommentDto;
import com.kzuromski.redditclone.dto.postcomment.PostCommentDto;
import com.kzuromski.redditclone.model.PostComment;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface PostCommentService {
    List<PostCommentDto> getPostComments(Long id);

    AddPostCommentDto addComment(Long postId, AddPostCommentDto addPostCommentDto);

    void deleteComment(Long postId, Long commentId);

    Optional<PostComment> getComment(Long postId, Long commentId);

    ResponseEntity<PostComment> editComment(Long postId, Long commentId, EditPostCommentDto editPostCommentDto);
}
