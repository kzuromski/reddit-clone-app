package com.kzuromski.redditclone.service;

import com.kzuromski.redditclone.dto.post.AddPostDto;
import com.kzuromski.redditclone.dto.post.EditPostDto;
import com.kzuromski.redditclone.dto.post.PostDto;
import com.kzuromski.redditclone.model.Post;

import java.util.List;
import java.util.Optional;

public interface PostService {
    Optional<List<PostDto>> getAllPosts();

    Optional<Post> findOneById(Long id);

    Post save(AddPostDto post);

    Optional<Post> updatePost(Long id, EditPostDto editPostDto);

    List<String> findAllTags();
}
