package com.kzuromski.redditclone.service;

import com.kzuromski.redditclone.dto.post.PostDto;
import com.kzuromski.redditclone.dto.tag.EditTagDto;
import com.kzuromski.redditclone.dto.tag.TagDto;
import com.kzuromski.redditclone.model.Tag;

import java.util.List;
import java.util.Optional;

public interface TagService {
    Optional<List<TagDto>> getAllTags();

    Tag addTag(TagDto tagDto);

    Optional<List<PostDto>> getAllTagPosts(String tagName);

    Optional<Tag> editTag(EditTagDto editTagDto, String tagName);
}
