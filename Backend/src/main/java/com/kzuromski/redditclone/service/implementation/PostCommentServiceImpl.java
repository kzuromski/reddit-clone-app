package com.kzuromski.redditclone.service.implementation;

import com.kzuromski.redditclone.dto.postcomment.AddPostCommentDto;
import com.kzuromski.redditclone.dto.postcomment.EditPostCommentDto;
import com.kzuromski.redditclone.dto.postcomment.PostCommentDto;
import com.kzuromski.redditclone.model.Post;
import com.kzuromski.redditclone.model.PostComment;
import com.kzuromski.redditclone.repository.PostCommentRepository;
import com.kzuromski.redditclone.repository.PostRepository;
import com.kzuromski.redditclone.service.PostCommentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PostCommentServiceImpl implements PostCommentService {
    private PostCommentRepository postCommentRepository;
    private PostRepository postRepository;
    private EntityManager entityManager;
    private ModelMapper modelMapper;

    @Autowired
    public PostCommentServiceImpl(PostCommentRepository postCommentRepository, PostRepository postRepository, EntityManager entityManager, ModelMapper modelMapper) {
        this.postCommentRepository = postCommentRepository;
        this.postRepository = postRepository;
        this.entityManager = entityManager;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<PostCommentDto> getPostComments(Long id) {

        Optional<Post> post = postRepository.findById(id);
        return post.map(comment -> post.get().getComments().stream().map(postComment -> modelMapper.map(postComment, PostCommentDto.class)).collect(Collectors.toList())).orElseThrow(NullPointerException::new);
    }

    @Override
    @Transactional
    public AddPostCommentDto addComment(Long postId, AddPostCommentDto addPostCommentDto) {
        Optional<Post> post = postRepository.findById(postId);
        if (post.isPresent()) {
            addPostCommentDto.setDate(new Date());
            post.get().addComment(modelMapper.map(addPostCommentDto, PostComment.class));
            entityManager.persist(post.get());
        }
        return addPostCommentDto;
    }

    @Override
    @Transactional
    public void deleteComment(Long postId, Long commentId) {
        Optional<Post> post = postRepository.findById(postId);
        if (post.isPresent()) {
            Optional<PostComment> comment = postCommentRepository.findById(commentId);
            comment.ifPresent(value -> post.get().removeComment(comment.get()));
        }
    }

    @Override
    public Optional<PostComment> getComment(Long postId, Long commentId) {
        Optional<Post> post = postRepository.findById(postId);
        if (post.isPresent()) {
            return post.get().getComments().stream().filter(postComment -> postComment.getId().equals(commentId)).findFirst();
        }
        return Optional.empty();
    }

    @Override
    @Transactional
    public ResponseEntity<PostComment> editComment(Long postId, Long commentId, EditPostCommentDto editPostCommentDto) {
        Optional<Post> postOptional = postRepository.findById(postId);
        if (!postOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        } else {
            Optional<PostComment> postCommentOptional = postCommentRepository.findById(commentId);
            if (!postCommentOptional.isPresent()) {
                return ResponseEntity.notFound().build();
            } else {
                PostComment postComment = new PostComment();
                postComment.setId(postCommentOptional.get().getId());
                postComment.setAuthor(postCommentOptional.get().getAuthor());
                postComment.setContent(editPostCommentDto.getContent());
                postComment.setDate(new Date());
                postOptional.get().removeComment(postCommentOptional.get());
                postOptional.get().addComment(postComment);
                entityManager.merge(postOptional.get());
            }
        }
        return ResponseEntity.noContent().build();
    }
}
