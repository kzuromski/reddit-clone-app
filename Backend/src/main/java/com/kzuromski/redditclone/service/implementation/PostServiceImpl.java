package com.kzuromski.redditclone.service.implementation;

import com.kzuromski.redditclone.dto.post.AddPostDto;
import com.kzuromski.redditclone.dto.post.EditPostDto;
import com.kzuromski.redditclone.dto.post.PostDto;
import com.kzuromski.redditclone.model.Post;
import com.kzuromski.redditclone.repository.PostRepository;
import com.kzuromski.redditclone.service.PostService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {
    private PostRepository postRepository;
    private ModelMapper modelMapper;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, ModelMapper modelMapper) {
        this.postRepository = postRepository;
        this.modelMapper = modelMapper;
    }

    public Optional<List<PostDto>> getAllPosts() {
        return Optional.of(postRepository.findAll().stream().map(post -> modelMapper.map(post, PostDto.class)).collect(Collectors.toList()));
    }

    public Optional<Post> findOneById(Long id) {
        return postRepository.findById(id);
    }

    public Post save(AddPostDto addPostDto) {
        addPostDto.setDate(new Date());
        return postRepository.save(modelMapper.map(addPostDto, Post.class));
    }

    @Override
    public Optional<Post> updatePost(Long id, EditPostDto editPostDto) {
        return postRepository.findById(id).map(post -> {
            post.setSubject(editPostDto.getSubject());
            post.setContent(editPostDto.getContent());
            post.setDate(new Date());
            return postRepository.save(post);
        });

    }


    public List<String> findAllTags() {
        //TO-DO
        return null;
    }
}
