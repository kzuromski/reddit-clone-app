package com.kzuromski.redditclone.service.implementation;

import com.kzuromski.redditclone.dto.post.PostDto;
import com.kzuromski.redditclone.dto.tag.EditTagDto;
import com.kzuromski.redditclone.dto.tag.TagDto;
import com.kzuromski.redditclone.model.Tag;
import com.kzuromski.redditclone.repository.TagRepository;
import com.kzuromski.redditclone.service.TagService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TagServiceImpl implements TagService {
    private TagRepository tagRepository;
    private ModelMapper modelMapper;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository, ModelMapper modelMapper) {
        this.tagRepository = tagRepository;
        this.modelMapper = modelMapper;
    }


    @Override
    public Optional<List<TagDto>> getAllTags() {
        return Optional.of(tagRepository.findAll().stream().map(tag -> modelMapper.map(tag, TagDto.class)).collect(Collectors.toList()));
    }

    @Override
    public Optional<List<PostDto>> getAllTagPosts(String tagName) {
        return tagRepository.findByName(tagName).map(tag -> tag.getPosts().stream().map(post -> modelMapper.map(post, PostDto.class)).collect(Collectors.toList()));
    }

    @Override
    public Optional<Tag> editTag(EditTagDto editTagDto, String tagName) {
        return tagRepository.findByName(tagName).map(tag -> {
            tag.setDescription(editTagDto.getDescription());
            return tagRepository.save(tag);
        });

    }

    @Override
    public Tag addTag(TagDto tagDto) {
        return tagRepository.save(modelMapper.map(tagDto, Tag.class));
    }


}
