import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostListComponent } from './post-list/post-list.component';
import { CreatePostComponent } from './create-post/create-post.component';
import { TagListComponent } from './tag-list/tag-list.component';
import { UserRegisterComponent } from './user-register/user-register.component';

const routes: Routes = [
    { path: '', redirectTo: 'posts', pathMatch: 'full'},
    { path: 'posts', component: PostListComponent},
    { path: 'posts/add', component: CreatePostComponent},
    { path: 'tags', component: TagListComponent},
    { path: 'signup', component : UserRegisterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
