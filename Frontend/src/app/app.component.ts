import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Frontend';
  navlinks = [
    { path: '', label: 'Posts'},
    { path: 'posts/add', label: 'New Post'},
    { path: 'tags', label: 'Tags'}
  ];
}
