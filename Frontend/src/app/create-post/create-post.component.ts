import { Component, OnInit } from '@angular/core';
import { Post } from './../post';
import { PostService } from './../post.service';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  post: Post = new Post();
  submitted = false;
  constructor(private postService: PostService) { }

  ngOnInit() {
  }
  newPost(): void {
      this.submitted = false;
      this.post = new Post();
  }
  save() {
      this.postService.createPost(this.post).subscribe(data => console.log(data), error => console.log(error));
      this.post = new Post();
  }
  onSubmit() {
      this.submitted = true;
      this.save();
  }
}
