import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private url: string;

  constructor(private http: HttpClient) {
      this.url = 'http://localhost:8080/posts'
  }

  getPost(id: number): Observable<Object> {
      return this.http.get(this.url + '/${id}');
  }

  createPost(post: Object): Observable<Object> {
      return this.http.post(this.url + '/add', post);
  }

  getPostList(): Observable<any> {
      return this.http.get(this.url);
  }
}
