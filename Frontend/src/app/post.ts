export class Post {
    id: number;
    tag: string;
    author: string;
    date: string;
    subject: string;
    content: string;
}
