import { Component, OnInit } from '@angular/core';
import { TagService } from './../tag.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.css']
})
export class TagListComponent implements OnInit {

   tags: Observable<String[]>;

  constructor(private tagService: TagService) { }

  ngOnInit() {
      this.reloadData();
  }
  reloadData() {
      this.tags = this.tagService.getTagList();
  }
}
