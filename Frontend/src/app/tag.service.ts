import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  private url: string;

  constructor(private http: HttpClient) {
      this.url = 'http://localhost:8080/tags'
  }

  getTagList(): Observable<any> {
      return this.http.get(this.url);
  }
  
  getTagPosts(name: string): Observable<any> {
      return this.http.get(this.url + "/${name}")
  }
}
