import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {

  user: User = new User();
  submitted = false;
  constructor(private userService: UserService) { }

  ngOnInit() {
  }
  newPost(): void {
      this.submitted = false;
      this.user = new User();
  }
  save() {
      this.userService.signup(this.user).subscribe(data => console.log(data), error => console.log(error));
      this.user = new User();
  }
  onSubmit() {
      this.submitted = true;
      this.save();
  }

}
