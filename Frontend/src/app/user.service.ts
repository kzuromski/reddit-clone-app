import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url: string;

  constructor(private http: HttpClient) {
      this.url = 'http://localhost:8080/api/auth'
  }

  signup(user: Object): Observable<Object> {
    return this.http.post(this.url + '/signup', user);
}
}
