### Reddit-clone-app

- Backend
  - Java 8 Spring Boot 2 MySQL
- Frontend
  - Todo

### Endpoints

**-api/post**
- **GET**: Returns all posts

**-api/post/add**
- **POST**: Adds new post

**-api/post/{id}**
- **GET**: Return post with given id
- **PUT**: Edits post with given id
- **DELETE**: Removes post with given id
- **POST**: Adds new comment to post with given id

**-api/post/{postId}/comments**
- **GET**: Returns all comment for post with given id

**-api/post/{postId}/comments/{commentId}**
- **GET**: Returns comment with given commentId that belongs to post with given postId
- **DELETE**: Deletes comment with given commentId that belongs to post with given postId
- **PUT**: Edits comment with given commentId that belongs to post with given postId

**-api/tag**
- **GET**: Returns all tags

**-api/tag/add**
- **POST**: Adds new tag

**-api/tag/{tagName}**
- **GET**: Returns all posts that belongs to tag with given tagName
- **POST**: Adds new post to tag with given tagName
- **PUT**: Edits tag description in tag with given tagName

**-api/auth/signup**
- **POST**: Creates new user

**-api/auth/signin**
- **POST**: Signs in user


##### Run
- `docker-compose up --build`
